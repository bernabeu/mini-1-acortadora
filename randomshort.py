from webApp import WebApp
import shelve
from urllib.parse import unquote
import random
import string

class ContentPOST(WebApp):
    def __init__(self, host, port):
        super().__init__(host, port)
        self.urls_dict = {}

    def parse(self, received):
        request = received.decode()
        url_name = request.split(' ')[1][0:]
        metodo = request.split(' ')[0]
        if metodo == "POST":
            body = request.split('\r\n\r\n')[1]
        else:
            body = None
        return {'metodo': metodo, 'url_name': url_name, 'body': body}

    def generar_url_corta(self, url):
        identificador = ''.join(random.choices(string.ascii_lowercase + string.digits, k=7))
        url = url.replace("url=", "")
        if url.startswith("http://") or url.startswith("https://"):
            url_corta = f"http://localhost:1234/{identificador}"
            self.urls_dict[url_corta] = url
        else:
            url_corta = f"http://localhost:1234/{identificador}"
            self.urls_dict[url_corta] = f"http://{url}"
            url = self.urls_dict[url_corta]
        return url_corta, url

    def read_shelve(self):
        with shelve.open('urls.db') as db:
            return dict(db)

    def lista_urls(self):
        self.urls_dict = self.read_shelve()
        if len(self.urls_dict) == 0:
            lista_urls_html = '<html><body><h1>Vaya! Parece que no hay ninguna url guardada</h1></body></html>'
        else:
            lista_urls_html = '<html><body><h1>Lista de URLS acortadas y guardadas anteriormente:</h1>'
            for clave, valor in self.urls_dict.items():
                lista_urls_html += f'<p> URL acortada: {clave} // URL real:{valor}</p>'
            lista_urls_html += '</body></html>'
        return lista_urls_html

    def do_GET(self, url_name, contenido):
        if f'http://localhost:1234{url_name}' in self.urls_dict.keys() and url_name != "/":
            http_code = "HTTP/1.1 301 Moved Permanently\r\n"
            html_content = f"Location: {self.urls_dict[f'http://localhost:1234{url_name}']}\r\n\r\n"
        elif f'http://localhost:1234{url_name}' not in self.urls_dict.keys() and url_name != "/":
            http_code = "404 NOT FOUND"
            html_content = '<html><body><h1>HTTP ERROR: Recurso no disponible</h1></body></html>'
        else:
            http_code = "200 OK"
            html_content = '<html><body><h1>' + contenido + '</h1></body></html>' + self.lista_urls()
        return http_code, html_content

    def do_POST(self, url_body, contenido):
        if url_body != 'url=':
            url_real = unquote(url_body)
            url_real = url_real.replace("url=", "")
            if url_real in self.urls_dict.values():
                http_code = "404 NOT FOUND"
                html_content = f'<html><body><h1>La url {url_real} ya ha sido cortada anteriormente.' \
                               f'</h1></body></html> ' + contenido + self.lista_urls()
            else:
                url_corta, url = self.generar_url_corta(url_real)
                db = shelve.open('urls.db')
                db[url_corta] = url
                db.close()
                http_code = "200 OK"
                html_content = '<html><body><h1>URL corta generada: ' + url_corta + contenido + \
                               '</h1></body></html>' + self.lista_urls()
        else:
            http_code = "404 NOT FOUND"
            html_content = '<html><body><h1> 404 Error: no se ha especificado una URL</h1></body></html>' + contenido
        return http_code, html_content

    def process(self, analyzed):
        url_name = analyzed['url_name']
        url_body = analyzed['body']
        metodo = analyzed['metodo']
        with open('formulario.html') as archivo:
            contenido = archivo.read()

        if metodo == 'GET':
            http_code, html_content = self.do_GET(url_name, contenido)
        else:
            http_code, html_content = self.do_POST(url_body, contenido)
        return http_code, html_content

if __name__ == '__main__':
    web_app = ContentPOST('', 1234)
    web_app.accept_clients()
